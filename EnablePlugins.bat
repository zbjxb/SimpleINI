@echo off
echo search .uplugin- files then change their file extensions
for /r %%u in (*.uplugin-) do (
    echo restore %%u
    move "%%u" "%%~dpnu.uplugin"
)
